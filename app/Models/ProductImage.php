<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProductImage extends Model
{
    protected $fillable = ['product_id','is_default','path'];

    public function getUrlAttribute()
    {
        return Storage::url($this->path);
    }
}
