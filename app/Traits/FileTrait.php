<?php

namespace App\Traits;

use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait FileTrait
{
    /**
     * saving single file using storage
     * @param File|UploadedFile $file
     * @param string $path
     * @param string|null $parent
     * @param string $disk
     * @return void
     */
    private function saveFile($file, string $path, $parent=null, $disk ='public')
    {
        $path = $parent ? "$path/$parent" : "$path" ;
        return Storage::disk($disk)->putFile($path, $file);
    }

    /**
     * deleting single directory using storage
     * @param string $path
     * @param string|null $child
     * @param string $disk
     * @return void
     */
    private function deleteDirectory(string $path, $child=null, $disk='public')
    {
        $path = $child ? "$path/$child" : $path ;
        Storage::disk($disk)->deleteDirectory($path);
    }

    /**
     * deleting single file using storage
     * @param string $path
     * @param string $disk
     * @return void
     */
    private function deleteFile(string $path, $disk='public')
    {
        if ($this->existFile($path))
            Storage::disk($disk)->delete($path);
    }

    /**
     * deleting single file using storage
     * @param string $path
     * @param string $disk
     * @return boolean
     */
    private function existFile(string $path, $disk='public')
    {
        return Storage::disk($disk)->exists($path);
    }
}
