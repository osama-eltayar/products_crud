<?php

namespace App\Observers;

use App\Classes\StorageTree;
use App\Models\Product;
use App\Events\ProductCreated;
use App\Notifications\ProductAdded;
use App\Traits\FileTrait;
use App\User;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class ProductObserver
{
    use FileTrait;
    /**
     * Handle the product "created" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        event(new ProductCreated($product));
        $users = User::role('user')->get();
        Notification::send($users, new ProductAdded($product));
    }

    

    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        $product->images()->delete();
        $this->deleteDirectory(StorageTree::PRODUCTS, $product->id);
    }
}
