<?php

namespace App\Observers;

use App\Models\ProductImage;
use App\Traits\FileTrait;

class ImageObserver
{
    use FileTrait;
    /**
     * Handle the product image "deleted" event.
     *
     * @param  \App\Models\ProductImage  $productImage
     * @return void
     */
    public function deleted(ProductImage $productImage)
    {
        $this->deleteFile($productImage->path);
    }
}
