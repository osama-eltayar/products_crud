<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['nullable','between:3,35'] ,
            'price' => ['nullable', 'numeric', 'min:1'] ,
            'description' => ['nullable', 'min:5'] ,
            'main_image' => ['nullable','image','between:5,512'] ,
            'images' => ['nullable','array'],
            'images.*' => ['nullable','image','between:5,512']
        ];
    }
}
