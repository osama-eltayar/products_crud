<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','between:3,35'] ,
            'price' => ['required', 'numeric', 'min:1'] ,
            'description' => ['required', 'min:5'] ,
            'main_image' => ['required','image','between:5,512'] ,
            'images' => ['nullable','array'],
            'images.*' => ['required','image','between:5,512']
        ];
    }
}
