<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Traits\FileTrait;
use App\Classes\StorageTree;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;

class ProductController extends Controller
{
    use FileTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:admin')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('mainImage')->latest()->get();
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $productData = $request->only(['name','description','price']);
        
        $product = Product::create($productData);

        $images = [];
        if ($request->has('main_image')) {
            $images[] = [
                'path' => $this->saveFile($request->main_image, StorageTree::PRODUCTS, $product->id),
                'is_default' => true
            ];
        }

        if ($request->has('images')) {
            foreach ($request->images as $image) {
                $images[] = [
                'path' => $this->saveFile($image, StorageTree::PRODUCTS, $product->id),
                ];
            }
        }

        $product->images()->createMany($images);
        return redirect(route('products.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product->load('images');
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product->load(['images' => function ($query) {
            $query->where('is_default', '0');
        } ]);
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $productData = $request->only(['name','description','price']);
        $product->update($productData);
        
        $images = [];
        if ($request->has('main_image')) {
            $product->mainImage->delete();
            $images[] = [
                'path' => $this->saveFile($request->main_image, StorageTree::PRODUCTS, $product->id),
                'is_default' => true
            ];
        }
        
        if ($request->has('images')) {
            foreach ($request->images as $image) {
                $images[] = [
                'path' => $this->saveFile($image, StorageTree::PRODUCTS, $product->id),
                ];
            }
        }
        $product->images()->createMany($images);

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json([
            'message' => 'deleted successful'
        ]);
    }
}
