<?php

namespace App\Providers;

use App\User;
use App\Models\Product;
use App\Models\ProductImage;
use App\Observers\UserObserver;
use App\Observers\ImageObserver;
use App\Observers\ProductObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Product::observe(ProductObserver::class);
        ProductImage::observe(ImageObserver::class);
        User::observe(UserObserver::class);
    }
}
