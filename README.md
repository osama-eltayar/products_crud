# products Crud

>assesment backend task for Ibtdi Company

**small laravel app to perform crud operation on product resource including real time** **updating when view products using websockets and laravel Echo**

## Install instructins

1. git clone https://osama-eltayar@bitbucket.org/osama-eltayar/products_crud.git
1. in terminal `$ cd /Products_crud`
1. in terminal  `$ composer install`
1. in terminal `$ npm install`
1. in terminal  `$ cp .env.example .env`
1. in terminal `$ php artisan key:generate`
1. configure your database in .env file
1. configure your email provider in .env file
1. in terminal  `$ npm run dev`
1. in terminal  `$ php artisan storage:link`
1. in terminal  `$ php artisan migrate --seed`
1. in terminal  `$ php artisan serve`
1. in terminal  `$ php artisan websocket:serve`

---------------------------------------------------

>guest user can view products only admin can crud
**by default you have admin account**

email : **superadmin@gmail.com**
password : **12345678**

---------------------------------------------------

>application by default use sync queue if you want use database queue to send notification in back-ground follow this

1. change QUEUE_CONNECTION=sync to QUEUE_CONNECTION=database in .env file
1. in terminal `$ php artisan queue:work`

---------------------------------------------------

>websockets by default use 6001 port if you want to change it define yours in .env
