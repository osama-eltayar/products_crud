<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'products');

Auth::routes();

Route::resource('products', 'ProductController');
Route::delete('images/{productImage}', 'ImageController@destroy')->name('images.destroy');
