@extends('layouts.app')

@section('content')
<div class="container">
    @auth
        @role('admin')
            <a href="{{route('products.create')}}" class="btn btn-dark">New Product</a>
        @endrole
    @endauth
    
    <div class="row " id="products-list">
        @forelse ($products as $product)
            <div class="col-lg-4 col-md-6 py-3 single-product">
                <h3 class="product-name">{{$product->name}}</h3>
                <p class="product-price">{{$product->price}}</p>
                <p class="product-description">{{$product->description}}</p>
                <div class="row">
                    <div class="col-4">
                        <a href="{{route('products.show',$product->id)}}" class="btn btn-primary product-show">show</a>
                    </div>
                    @auth
                        @role('admin')
                            <div class="col-4">
                                <a href="{{route('products.destroy',$product->id)}}" class="btn btn-danger product-destroy">delete</a>
                            </div>
                            <div class="col-4">
                                <a href="{{route('products.edit',$product->id)}}" class="btn btn-success product-edit">edit</a>
                            </div>
                        @endrole
                    @endauth
                </div>
            </div>
        @empty
            <div class="col-lg-4 col-md-6 py-3 single-product" style="display: none">
                <h3 class="product-name"></h3>
                <p class="product-price"></p>
                <p class="product-description"></p>
                <div class="row">
                    <div class="col-4">
                        <a href="" class="btn btn-primary product-show">show</a>
                    </div>
                    @auth
                        @role('admin')
                            <div class="col-4">
                                <a href="" class="btn btn-danger product-destroy">delete</a>
                            </div>
                            <div class="col-4">
                                <a href="" class="btn btn-success product-edit">edit</a>
                            </div>
                        @endrole
                    @endauth
                </div>
            </div>
        @endforelse
    </div>
</div>
@endsection
@section('script')
    <script src="{{asset('js/app.js')}}"></script>
<script>
    $(document).ready(()=>{

        // listen to new products
        subscribeToProducts()

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click','.product-destroy',(e)=>{
            e.preventDefault();
            let selector = $(e.target) ;
            let url = selector.attr('href')
            let target = selector.closest('.single-product')

            destroyProduct(url,target)
        });


    });

    function subscribeToProducts()
    {
        Echo.channel('products')
            .listen('.product.new', (e) => {
                console.log(e.product);
                appendProduct(e.product)
            });
    }


    function appendProduct(product)
    {
        // prepare new product links
        let showLink = "{{route('products.show','id')}}"
        let editLink = "{{route('products.edit','id')}}"
        showLink = showLink.replace("id", product.id);
        editLink = editLink.replace("id", product.id);

        // prepare new product div
        let cloned = $('.single-product:first').clone().show()
        cloned.find('.product-name').text(product.name)
        cloned.find('.product-price').text(product.price)
        cloned.find('.product-description').text(product.description)
        cloned.find('.product-show').attr('href',showLink)
        cloned.find('.product-edit').attr('href',editLink)
        cloned.find('.product-destroy').attr('href',showLink)
        $('#products-list').prepend(cloned);
    }

    function destroyProduct(url,target)
    {
         $.ajax({
                method: "delete",
                url :url,
            })
            .done(function(response ) {
                target.remove()
            });
    }
</script>

@endsection
