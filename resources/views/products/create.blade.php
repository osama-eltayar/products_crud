@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="name">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="price">price</label>
            <input type="number" min="0" class="form-control" name="price" id="price" value="{{ old('price') }}" placeholder="price">
            @error('price')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="description">description</label>
            <textarea type="text" min="0" class="form-control" name="description"  id="description" placeholder="description">{{ old('description') }}</textarea>
            @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="main-image">product main image</label>
            <input type="file" class="form-control-file" id="main-image" name="main_image">
            @error('main_image')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="images">Product images</label>
            <input type="file" class="form-control-file" id="images" name="images[]" multiple>
            @error('images')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            @error('images.*')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

@endsection