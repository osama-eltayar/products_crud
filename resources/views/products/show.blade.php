@extends('layouts.app')

@section('content')
<div class="container">
        <h3>{{$product->name}}</h3>
        <p>{{$product->price}}</p>
        <p>{{$product->description}}</p>
        <div class="row">
            @foreach ($product->images as $image)
                <div class="col-4">
                    <img src="{{$image->url}}" style="max-height: 100%; max-width:100%" alt="">
                </div>
            @endforeach
        </div>
</div>
@endsection