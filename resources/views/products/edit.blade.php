@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{route('products.update',$product->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="name" value="{{$product->name}}">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="price">price</label>
            <input type="number" min="0" class="form-control" name="price" id="price" placeholder="price" value="{{$product->price}}">
            @error('price')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="description">description</label>
            <input type="text" min="0" class="form-control" name="description" id="description" value="{{$product->description}}" placeholder="description">
            @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="main-image">main-image</label>
            <input type="file" class="form-control-file" id="main-image" name="main_image">
            @error('main_image')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="row">
            @foreach ($product->images as $image)
                <div class="col-4 single-image">
                    <img src="{{$image->url}}" style="max-height: 100%; max-width:100%; display:block" alt="">
                    <a href="{{route('images.destroy',$image->id)}}" class="btn btn-danger image-destroy">delete</a>
                </div>
            @endforeach
        </div>
        <div class="form-group">
            <label for="images">images</label>
            <input type="file" class="form-control-file" id="images" name="images[]" multiple>
            @error('images')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            @error('images.*')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

@endsection

@section('script')
<script>
    $(document).ready(()=>{

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click','.image-destroy',(e)=>{
            e.preventDefault();
            let selector = $(e.target) ;
            let url = selector.attr('href')
            let target = selector.closest('.single-image')
           
            destroyImage(url,target)
        });
        

    });

    function destroyImage(url,target)
    {
         $.ajax({
                method: "delete",
                url :url,
            })
            .done(function(response ) {
                target.remove()
            });
    }
</script>
    
@endsection