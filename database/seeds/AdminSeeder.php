<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User ;
        $admin->unsetEventDispatcher();
        $admin = $admin->firstOrCreate(
            ["email"=>"superadmin@gmail.com"],
            [
            "name"=>"superAdmin",
            "email"=>"superadmin@gmail.com",
            "password"=>Hash::make("12345678")
        ]
        );
        
        if ($admin->wasRecentlyCreated) {
            $admin->assignRole('admin');
        }
    }
}
